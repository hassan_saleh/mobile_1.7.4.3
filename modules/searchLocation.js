function onClickSearch(flag,rightVal){
  kony.print("@@inside onClickSearch..."+flag+", "+rightVal);
  try{
    frmLocatorKA.locatorSearchTextField.text = "";
    if(flag===true)
    {
      frmLocatorKA.btnSearch.setVisibility(true);
      frmLocatorKA.lblMapHeading.setVisibility(true);//false
      frmLocatorKA.searchContainer.setVisibility(false);
    }
    else
    {
      frmLocatorKA.btnSearch.setVisibility(false);
      frmLocatorKA.lblMapHeading.setVisibility(false);//false
      frmLocatorKA.searchContainer.setVisibility(true);
      gblSearchOn = true;
    }
//     frmLocatorKA.lblMapHeading.setVisibility(flag);//false
//     frmLocatorKA.searchContainer.setVisibility(!flag);//true
    frmLocatorKA.cancelButton.right = rightVal;//"0%";
  }catch(exceptionObject){
    kony.print("Exception found in onClickSearch:::"+JSON.stringify(exceptionObject));
  }
}

function onLoadClickCancelSearch(){
  kony.print("@@inside onClickCancelSearch...");
  try{
    //searchStringmap("");
    frmLocatorKA.locatorSearchTextField.text = "";
    gblmapSearchKeypad = false;
	searchStringmap(frmLocatorKA.locatorSearchTextField.text);
    gblSearchOn = false;
    frmLocatorKA.btnSearch.setVisibility(true);
    onClickSearch(GENERIC_COSNTATNTS.TRUE,"-15%");
  }catch(exceptionObject){
    kony.print("Exception found in onClickCancelSearch:::"+JSON.stringify(exceptionObject));
  }
}

function onSearchLocation(){
  kony.print("@@inside onSearchLocation...");
  var searchKey = frmLocatorKA.locatorSearchTextField.text;
  try{
    if(!isNullorEmptyorUndefined(searchKey)){
      findSegData(searchKey);
    }else{
      frmLocatorKA.locatorSegmentList.removeAll();
      if(gblCurrentMapType == MAP_CONSTANTS.ALL){
        frmLocatorKA.locatorSegmentList.data = gblLocationData;
      }else if(gblCurrentMapType == MAP_CONSTANTS.BRANCH){
        frmLocatorKA.locatorSegmentList.data = gblLocationDataBranch;
      }else{
        frmLocatorKA.locatorSegmentList.data = gblLocationDataATM;
      }
    }
  }catch(exceptionObject){
    kony.print("Exception found in onSearchLocation:::"+JSON.stringify(exceptionObject));
  }
}

function findSegData(searchKey){
  kony.print("@@inside findSegData...");
  var segData = "";
  var searchDataArr = [];
  try{
    if(frmLocatorKA.locatorSegmentList != null || frmLocatorKA.locatorSegmentList != undefined){
      segData =   frmLocatorKA.locatorSegmentList.data;
    }
    for(var n = 0; n<segData.length; n++){
      if(segData[n].name.indexOf(searchKey)>-1){
        searchDataArr.push(segData[n]);
      }
    }
    kony.print("Data found :::"+JSON.stringify(searchDataArr));
    setSearchData(searchDataArr);
  }catch(exceptionObject){
    kony.print("Exception found in findSegData:::"+JSON.stringify(exceptionObject));
  }
}

function setSearchData(dataArr){
  kony.print("@@inside setSearchData...");
  //frmLocatorKA.LabelNoRecordsKA.text  = kony.i18n.getLocalizedString("i18n.commomerror.dataNotFound");
  try{
    if(data.length>0){
      frmLocatorKA.LabelNoRecordsKA.setVisibility(false);
      frmLocatorKA.locatorSegmentList.removeAll();
      frmLocatorKA.locatorSegmentList.data = dataArr;
      frmLocatorKA.locatorSegmentList.setVisibility(true);
    }else{
      frmLocatorKA.locatorSegmentList.setVisibility(false);
      frmLocatorKA.LabelNoRecordsKA.text  = kony.i18n.getLocalizedString("i18n.commomerror.dataNotFound");
      frmLocatorKA.LabelNoRecordsKA.setVisibility(true);
    }
  }catch(exceptionObject){
    kony.print("Exception found in setSearchData:::"+JSON.stringify(exceptionObject));
  };
}


function setLocationDetails(segData){
  kony.print("@@inside setLocationDetails");
  try{
    kony.print("selected data::"+JSON.stringify(segData));
    kony.print("selected length::"+segData.length);
    clearDetails();
    gblSelectionPinData = {"lon":segData.lon,"lat":segData.lat};
    if(!isNullorEmptyorUndefined(segData)){
      frmLocationDetailsKA.lblTitle.text =  kony.i18n.getLocalizedString("i18n.manage_payee.details");
      frmLocationDetailsKA.lblName.text =  segData.name;
      frmLocationDetailsKA.lblType.text = segData.calloutData.rbType;
      frmLocationDetailsKA.lblMobileNumber.text = segData.phone;
      frmLocationDetailsKA.lblExtVal.text = segData.placeID;
      frmLocationDetailsKA.lblFaxVal.text = segData.fax;
      frmLocationDetailsKA.lblAddressLine1.text = segData.address1;
      frmLocationDetailsKA.lblAddressLine2.text = segData.address2;
      frmLocationDetailsKA.lblCity.text = segData.city;
    }
    if(segData.calloutData.rbType != "" && segData.calloutData.rbType == kony.i18n.getLocalizedString("i18n.locateus.btnATM")){
      frmLocationDetailsKA.lblNameTitle.text = kony.i18n.getLocalizedString("i18n.map.ATMname");

      frmLocationDetailsKA.lblMobile.setVisibility(false);
      frmLocationDetailsKA.lblMobileNumber.setVisibility(false);
      frmLocationDetailsKA.flxTeleBorder.setVisibility(false);
      frmLocationDetailsKA.lblExt.setVisibility(false);
      frmLocationDetailsKA.lblExtVal.setVisibility(false);
      frmLocationDetailsKA.flxextBorder.setVisibility(false);
      frmLocationDetailsKA.lblFax.setVisibility(false);
      frmLocationDetailsKA.lblFaxVal.setVisibility(false);
      frmLocationDetailsKA.flxFaxBorder.setVisibility(false);

    }else if(segData.calloutData.rbType !== "" && (segData.calloutData.rbType === kony.i18n.getLocalizedString("i18n.locateus.branch") ||
                                                  segData.calloutData.rbType === kony.i18n.getLocalizedString("i18n.locateus.branch").toUpperCase())){
      frmLocationDetailsKA.lblNameTitle.text = kony.i18n.getLocalizedString("i18n.Map.Branchname");

      frmLocationDetailsKA.lblMobile.setVisibility(true);
      frmLocationDetailsKA.lblMobileNumber.setVisibility(true);
      frmLocationDetailsKA.flxTeleBorder.setVisibility(true);
      frmLocationDetailsKA.lblExt.setVisibility(true);
      frmLocationDetailsKA.lblExtVal.setVisibility(true);
      frmLocationDetailsKA.flxextBorder.setVisibility(true);
      frmLocationDetailsKA.lblFax.setVisibility(true);
      frmLocationDetailsKA.lblFaxVal.setVisibility(true);
      frmLocationDetailsKA.flxFaxBorder.setVisibility(true);
    }else{
      // dont know wether the atm or branch
      frmLocationDetailsKA.lblNameTitle.text = kony.i18n.getLocalizedString("i18n.Bene.Bankname");

      frmLocationDetailsKA.lblMobile.setVisibility(true);
      frmLocationDetailsKA.lblMobileNumber.setVisibility(true);
      frmLocationDetailsKA.flxTeleBorder.setVisibility(true);
      frmLocationDetailsKA.lblExt.setVisibility(true);
      frmLocationDetailsKA.lblExtVal.setVisibility(true);
      frmLocationDetailsKA.flxextBorder.setVisibility(true);
      frmLocationDetailsKA.lblFax.setVisibility(true);
      frmLocationDetailsKA.lblFaxVal.setVisibility(true);
      frmLocationDetailsKA.flxFaxBorder.setVisibility(true);
    }
    frmLocationDetailsKA.show();
  }catch(ex){
    kony.print("Exception found in setLocationDetails::"+JSON.stringify(ex));
  }
}

function clearDetails(){
  kony.print("@@inside clearDetails");
  try{
    frmLocationDetailsKA.lblTitle.text = "";
    frmLocationDetailsKA.lblName.text =  "";
    frmLocationDetailsKA.lblType.text = "";
    frmLocationDetailsKA.lblMobileNumber.text = "";
    frmLocationDetailsKA.lblExtVal.text = "";
    frmLocationDetailsKA.lblFaxVal.text = "";
    frmLocationDetailsKA.lblAddressLine1.text = "";
    frmLocationDetailsKA.lblAddressLine2.text = "";
    frmLocationDetailsKA.lblCity.text = "";
    frmLocationDetailsKA.show();
  }catch(ex){
    kony.print("Exception found in clearDetails::"+JSON.stringify(ex));
  }
}
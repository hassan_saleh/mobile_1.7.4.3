var pinSetType;
var fromSettings=false;
function getPinSettingsStatus()
{
    if(kony.retailBanking.globalData.globals.userObj.isPinSet == "true")
    {
      if(isSettingsFlagEnabled("isPinEnabledFlag"))
       {
            pinSetType=1;
      		isPinEnabled=i18n_onLabel;
       }
      else
       {
          pinSetType=2;
          isPinEnabled=i18n_offLabel;
    	}
    }
    else
      {
        pinSetType=0;   
        isPinEnabled=i18n_offLabel;
      }

}

function gotoPinSettings()
{
  getPinSettingsStatus();
  if (pinSetType === 0)
  {
     
       if(isSettingsFlagEnabled("rememberMeFlag"))
        {
          if(isSettingsFlagEnabled("deviceRegisterFlag"))
          {
            frmSetUpPinSettings.show();
          }
         
        }
        
  }
  
}

function gotoFaceIdSettings()
{

    frmSetupFaceIdSettingsKA.show();
}



function showRegisterDeviceAlert(msg,reserPinFunction)
{
  kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "INFO",
        "yesLabel": "OK",
        "noLabel": "",
        "message": msg,
        "alertHandler": reserPinFunction
     },{});              

}    



function navigatetoUpdatePin()
{
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listcontroller = INSTANCE.getFormController("frmPinEditFromSettings");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
    listcontroller.performAction("loadDataAndShowForm",[navObject]);
}

function onClickResetPinSettings()
{
  	  var INSTANCE =  kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var controller = INSTANCE.getFormController("frmPinEditFromSettings");
     var viewModel = controller.getFormModel();
     var oldPin = viewModel.getViewAttributeByProperty("txtOldPin", "text");
  	 var newPin = viewModel.getViewAttributeByProperty("txtNewPin", "text");
     var ReEnteredPin = viewModel.getViewAttributeByProperty("txtPinReEnter", "text");
      if(oldPin === null || newPin === null || ReEnteredPin === null || oldPin=== ""|| newPin=== ""|| ReEnteredPin=== "")
     	 alert("Enter all the fields"); 
    else if(oldPin.length == 6 && newPin.length == 6 && ReEnteredPin.length == 6 && oldPin!=newPin && newPin===ReEnteredPin && checkUniquenss(newPin) && checkConsicutives(newPin)){       
          var navObject = new kony.sdk.mvvm.NavigationObject();
          navObject.setDataModel(null, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
          navObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
          controller.performAction("saveData",[navObject]);
    }
  else{
    alert(i18n_invalidNewPin);
  }
}

function updatePinPreShow(){
   var INSTANCE =  kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var controller = INSTANCE.getFormController("frmPinEditFromSettings");
     var viewModel = controller.getFormModel();
     var oldPin = viewModel.setViewAttributeByProperty("txtOldPin", "text","");
  	 var newPin = viewModel.setViewAttributeByProperty("txtNewPin", "text","");
     var ReEnteredPin = viewModel.setViewAttributeByProperty("txtPinReEnter", "text","");
    
}

function checkUniquenss(str){
   var splitString = str.split(""); 
   var reverseArray = splitString.reverse();
   var joinArray = reverseArray.join("");
   var result=Math.abs(Number(str)-Number(joinArray));
  if(result===Number("530865")){
        return false;
 	}
 	return true;
}

function checkConsicutives(str){
   var splitString = str.split(""); 
   var reverseArray = splitString.reverse();
   var joinArray = reverseArray.join("");
   var result=Math.abs(Number(str)-Number(joinArray));
   if(result===Number("0")){
        return false;
 	}
 	return true;
}
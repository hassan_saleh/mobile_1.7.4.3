function AS_Button_07065ee5644946d7a9a0e9e5c5f6befb(eventobject) {
    function SHOW_ALERT_ide_onClick_4cb47b7583c84c03ae19e2986036937b_True() {
        userAllowedLocation();
    }

    function SHOW_ALERT_ide_onClick_4cb47b7583c84c03ae19e2986036937b_False() {}

    function SHOW_ALERT_ide_onClick_4cb47b7583c84c03ae19e2986036937b_Callback(response) {
        if (response == true) {
            SHOW_ALERT_ide_onClick_4cb47b7583c84c03ae19e2986036937b_True()
        } else {
            SHOW_ALERT_ide_onClick_4cb47b7583c84c03ae19e2986036937b_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "Current Location",
        "yesLabel": "Allow",
        "noLabel": "Don't Allow",
        "message": "xyzBank would like to use your current location",
        "alertHandler": SHOW_ALERT_ide_onClick_4cb47b7583c84c03ae19e2986036937b_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}
function backToaccountDashboardfromAccountDetail(eventobject, x, y) {
    return AS_Label_c71cd08657254db5ab3ca89bb1e72c54(eventobject, x, y);
}

function AS_Label_c71cd08657254db5ab3ca89bb1e72c54(eventobject, x, y) {
    frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
    frmAccountsLandingKA.flxDeals.setVisibility(false);
    frmAccountsLandingKA.flxLoans.setVisibility(false);
    frmAccountsLandingKA.forceLayout();
    kony.retailBanking.globalData.globals.accountDashboardFlowFlag = false;
    if (frmAccountDetailKA.flxAccountdetailsSortContainer.top != "100%") {
        animatePopup_Sort();
    }
    frmAccountDetailKA.imgSortDateAsc.src = "map_arrow_straight_nonselected.png";
    frmAccountDetailKA.imgSortDateDesc.src = "map_arrow_down_nonselected.png";
    frmAccountDetailKA.imgSortAmountAsc.src = "map_arrow_straight_nonselected.png";
    frmAccountDetailKA.imgSortAmountDesc.src = "map_arrow_down_nonselected.png";
    accountDashboardDataCall();
    kony.print("Nav transaction to Dadhboard:::");
}
function AS_onClickQuickPay(eventobject) {
    return AS_Button_ga3baceb590e4968a0e1e0aca876a6ff(eventobject);
}

function AS_Button_ga3baceb590e4968a0e1e0aca876a6ff(eventobject) {
    kony.store.setItem("denoFlag", {});
    frmNewBillKA.btnPrePaid.skin = "slButtonWhiteTabDisabled";
    frmNewBillKA.btnPostPaid.skin = "slButtonWhiteTab";
    gblQuickFlow = "post";
    onClickPostPaidQuickPay();
    kony.boj.BillerResetFlag = true;
    kony.boj.selectedBillerType = "PostPaid";
    clearNewBillScreen();
    frmNewBillKA.show();
}
function AS_ConfirmPayBills(eventobject) {
    return AS_Button_eebc6476b6564ca0940c8118512a7b0d(eventobject);
}

function AS_Button_eebc6476b6564ca0940c8118512a7b0d(eventobject) {
    // assignDatatoConfirmPostpaidBill();//function to call--Arpan
    if (validate_BILLPAY_BILLER()) {
        if (validate_SufficientBalance()) {
            if (validate_MINMAXAMOUNTBILLPAYMENT() === "min") {
                var minAmount = "";
                minAmount = geti18Value("i18n.billPaymentminAmount") + " (" + frmBills.minAmount.text + ")";
                customAlertPopup(geti18Value("i18n.maps.Info"), minAmount, popupCommonAlertDimiss, "");
            } else if (validate_MINMAXAMOUNTBILLPAYMENT() === "max") {
                var maxAmount = "";
                maxAmount = geti18Value("i18n.billPaymentmaxAmount") + " (" + frmBills.maxAmount.text + ")";
                customAlertPopup(geti18Value("i18n.maps.Info"), maxAmount, popupCommonAlertDimiss, "");
            } else {
                if (gblQuickFlow === "post") {
                    frmConfirmPayBill.flxSourceofFunding.setVisibility(true);
                    frmConfirmPayBill.flxSourceofFundAccount.setVisibility(true);
                    var tempAMOUNT = "" + frmBills.tbxAmount.text;
                    tempAMOUNT = tempAMOUNT.replace(/,/g, "");
                    gblQuickFlow = "postBill";
                    serv_BILLSCOMISSIONCHARGE(tempAMOUNT);
                    nextfrmPostPaidBill();
                } else {
                    nextfrmPrePaidBill();
                }
                QuickFlowprev = "bill";
            }
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        }
    }
}
function AS_TextField_e3244cdda11440d58b74d7110a589278(eventobject, changedtext) {
    frmEditPayeeKA.txtBillerName.text = frmEditPayeeKA.txtBillerName.text.trim();
    if (frmEditPayeeKA.txtBillerName.text !== null && frmEditPayeeKA.txtBillerName.text !== "") {
        frmEditPayeeKA.lblUnderline.skin = "sknFlxGreenLine";
    } else {
        frmEditPayeeKA.lblUnderline.skin = "sknFlxOrangeLine";
    }
}
function onClickBackFromCardstmt(eventobject) {
    return AS_FlexContainer_a755307036184755ac5fdb7f4b75b5df(eventobject);
}

function AS_FlexContainer_a755307036184755ac5fdb7f4b75b5df(eventobject) {
    if (gblDownloadPDFFlow) {
        if (frmCardStatementKA.flxAccountdetailsSortContainer.top === "8%") animatePopup_Sort();
        kony.application.getPreviousForm().show();
    } else {
        if (frmCardStatementKA.flxCardStatement.isVisible === false) {
            if (frmCardStatementKA.flxCardsSegmentOption.top === "8%") animate_cardsFilterOption();
            frmCardStatementKA.flxCardStatement.isVisible = true;
            frmCardStatementKA.flxCardsFilterScreen.isVisible = false;
            frmCardStatementKA.flxFilterDone.isVisible = false;
        } else {
            if (frmCardStatementKA.flxAccountdetailsSortContainer.top === "8%") animatePopup_Sort();
            kony.application.getPreviousForm().show();
        }
    }
}
function AS_TextField_e9288e87d81c4499834424923a3d70c6(eventobject, changedtext) {
    if (frmRegisterUser.txtCardNum.text !== null && frmRegisterUser.txtPIN.text !== null && frmRegisterUser.txtMobileNumber.text !== null) {
        var cardNo = getCardNum(frmRegisterUser.txtCardNum.text);
        if (cardNo.length == 16 && frmRegisterUser.txtPIN.text.length == 4) {
            frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
        }
    } else {
        frmRegisterUser.btnMobileNumberConfirm.skin = "slButtonWhite";
    }
    if (!isEmpty(frmRegisterUser.txtMobileNumber.text)) {
        frmRegisterUser.CopyflxLine0a4214bb8a7c146.skin = "sknFlxGreenLine";
        frmRegisterUser.lblMobileNumberTitle.setVisibility(true);
        frmRegisterUser.lblMobileNumberClear.setVisibility(true);
    } else {
        frmRegisterUser.CopyflxLine0a4214bb8a7c146.skin = "sknFlxGreyLine";
        frmRegisterUser.lblMobileNumberTitle.setVisibility(false);
        frmRegisterUser.lblMobileNumberClear.setVisibility(false);
    }
}
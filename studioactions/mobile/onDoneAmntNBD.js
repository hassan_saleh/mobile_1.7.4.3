function onDoneAmntNBD(eventobject, changedtext) {
    return AS_TextField_e9766bcb83ec4c5e8537e028cb5e94fe(eventobject, changedtext);
}

function AS_TextField_e9766bcb83ec4c5e8537e028cb5e94fe(eventobject, changedtext) {
    var amount = frmNewBillDetails.tbxAmount.text;
    frmNewBillDetails.lblHiddenAmount.text = frmNewBillDetails.tbxAmount.text;
    if (amount === null || amount === "" || amount == "." || (Number(amount) < 0.01) || amount === 0.00) {} else {
        frmNewBillDetails.tbxAmount.text = restrictoThreeDecimals(frmNewBillDetails.tbxAmount.text);
        gblQuickFlow = "postBillAcc";
        serv_BILLSCOMISSIONCHARGE(frmNewBillDetails.tbxAmount.text.replace(/,/g, ""));
    }
}
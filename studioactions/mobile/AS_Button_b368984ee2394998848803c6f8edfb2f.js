function AS_Button_b368984ee2394998848803c6f8edfb2f(eventobject) {
    function SHOW_ALERT_ide_onClick_a49d9d32bbc7434694f3f9a325b3c670_True() {
        frmAccountsLandingKA.flxAccntLandingNavOptKA.isVisible = false;
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_a49d9d32bbc7434694f3f9a325b3c670_False() {}

    function SHOW_ALERT_ide_onClick_a49d9d32bbc7434694f3f9a325b3c670_Callback(response) {
        if (response == true) {
            SHOW_ALERT_ide_onClick_a49d9d32bbc7434694f3f9a325b3c670_True()
        } else {
            SHOW_ALERT_ide_onClick_a49d9d32bbc7434694f3f9a325b3c670_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_a49d9d32bbc7434694f3f9a325b3c670_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}
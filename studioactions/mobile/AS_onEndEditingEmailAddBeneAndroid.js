function AS_onEndEditingEmailAddBeneAndroid(eventobject, changedtext) {
    return AS_TextField_g73174f11e1845008bdeed5e297af2e1(eventobject, changedtext);
}

function AS_TextField_g73174f11e1845008bdeed5e297af2e1(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxEmail.text !== "") {
        frmAddExternalAccountKA.flxUnderlineEmail.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineEmail.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblEmail", frmAddExternalAccountKA.tbxEmail.text);
}
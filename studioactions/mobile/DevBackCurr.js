function DevBackCurr(eventobject) {
    return AS_Form_e19a8a0da531422685eb2d7dc36c4075(eventobject);
}

function AS_Form_e19a8a0da531422685eb2d7dc36c4075(eventobject) {
    if (gblCalculateFXLeft || gblCalculateFXRight || gblExchangeRatesFlag) {
        frmFxRate.show();
    } else if (gblTModule === "countryCode") {
        frmRegisterUser.show();
        frmCurrency.destroy();
    } else if (gblTModule === "loanPostpone") {
        frmLoanPostpone.show();
        frmCurrency.destroy();
        gblTModule = "";
    } else {
        frmNewTransferKA.show();
    }
}
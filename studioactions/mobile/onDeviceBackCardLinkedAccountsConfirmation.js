function onDeviceBackCardLinkedAccountsConfirmation(eventobject) {
    return AS_Form_dde306b8260947ad905111b4b9214c71(eventobject);
}

function AS_Form_dde306b8260947ad905111b4b9214c71(eventobject) {
    if (frmCardLinkedAccountsConfirm.btnConfirm.text === geti18Value("i18n.transfers.CONFIRM")) {
        frmCardLinkedAccounts.show();
        frmCardLinkedAccountsConfirm.destroy();
    }
}
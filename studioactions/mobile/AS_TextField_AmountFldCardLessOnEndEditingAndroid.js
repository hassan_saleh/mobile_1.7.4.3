function AS_TextField_AmountFldCardLessOnEndEditingAndroid(eventobject, changedtext) {
    return AS_TextField_jaacaadd54ed4fd5a190a9b884609885(eventobject, changedtext);
}

function AS_TextField_jaacaadd54ed4fd5a190a9b884609885(eventobject, changedtext) {
    onendeditingCaredless();
    frmCardlessTransaction.txtFieldAmount.maxTextLength = 20;
    frmCardlessTransaction.txtFieldAmount.text = formatAmountwithcomma(frmCardlessTransaction.lblHiddenAmount.text, getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
}
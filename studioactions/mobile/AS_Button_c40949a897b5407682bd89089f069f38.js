function AS_Button_c40949a897b5407682bd89089f069f38(eventobject) {
    function SHOW_ALERT_ide_onClick_h1d1d6f465ff404db8f0e2b6f70ee159_True() {
        //closeAndroidNav();
        //deviceRegFrom = "logout";
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_h1d1d6f465ff404db8f0e2b6f70ee159_False() {}

    function SHOW_ALERT_ide_onClick_h1d1d6f465ff404db8f0e2b6f70ee159_Callback(response) {
        if (response == true) {
            SHOW_ALERT_ide_onClick_h1d1d6f465ff404db8f0e2b6f70ee159_True()
        } else {
            SHOW_ALERT_ide_onClick_h1d1d6f465ff404db8f0e2b6f70ee159_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_h1d1d6f465ff404db8f0e2b6f70ee159_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}
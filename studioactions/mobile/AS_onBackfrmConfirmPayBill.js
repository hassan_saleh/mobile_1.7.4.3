function AS_onBackfrmConfirmPayBill(eventobject) {
    return AS_FlexContainer_e85df2fc7eb644a0bb8bc63567231b6c(eventobject);
}

function AS_FlexContainer_e85df2fc7eb644a0bb8bc63567231b6c(eventobject) {
    if (QuickFlowprev == "bill") frmBills.show();
    else if (gblQuickFlow === "pre") frmNewBillKA.show();
    else frmNewBillDetails.show();
}
function BOJPostAppInitAction(eventobject) {
    return AS_AppEvents_a1f7494243ba459d87b81e142176930a(eventobject);
}

function AS_AppEvents_a1f7494243ba459d87b81e142176930a(eventobject) {
    try {
        kony.print("Inside of BOJPostAppInitAction :: ");
        if (kony.sdk.isNetworkAvailable()) {
            isLoggedIn = false;
            kony.lang.setUncaughtExceptionHandler(uncaughtExceptionHandler);
            kony.application.setApplicationBehaviors({
                "hideDefaultLoadingIndicator": true
            });
            kony.application.setApplicationProperties({
                statusBarColor: "000000",
                statusBarForegroundColor: "ffffff",
                statusBarHidden: false,
                statusBarStyle: constants.STATUS_BAR_STYLE_LIGHT_CONTENT
            });
            layoutManagerInit();
            var langFlag = kony.store.getItem("langFlagSettingObj");
            if (langFlag) {
                var langSelected = kony.store.getItem("langPrefObj");
                if (langSelected === "en") {
                    onClickEnglish("en");
                } else {
                    onClickArabic("ar");
                }
                checkme = true;
            }
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.common.appalaunchnointernet"), popupCommonAlertDimissclose, "");
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }
    } catch (err) {
        kony.print("In catch of BOJPostAppInitAction :: " + err);
    }
}
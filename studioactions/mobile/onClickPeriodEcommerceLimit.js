function onClickPeriodEcommerceLimit(eventobject) {
    return AS_Button_a10c970a784842da835dc1f056b63cdf(eventobject);
}

function AS_Button_a10c970a784842da835dc1f056b63cdf(eventobject) {
    if (eventobject.id === "btnDaily") {
        onSelect_PERIOD_ECOMMERCELIMIT("daily");
    } else if (eventobject.id === "btnWeekly") {
        onSelect_PERIOD_ECOMMERCELIMIT("weekly");
    } else if (eventobject.id === "btnMonthly") {
        onSelect_PERIOD_ECOMMERCELIMIT("Monthly");
    }
}
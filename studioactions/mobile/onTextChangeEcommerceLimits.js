function onTextChangeEcommerceLimits(eventobject, changedtext) {
    return AS_TextField_j2f063e57eb14c58925fd4f626502832(eventobject, changedtext);
}

function AS_TextField_j2f063e57eb14c58925fd4f626502832(eventobject, changedtext) {
    if (eventobject.id === "txtEcomLimit") {
        onTextChange_ECOMMERCELIMIT(frmEnableInternetTransactionKA.txtEcomLimit, frmEnableInternetTransactionKA.sliderECommerceLmt, "onTextChange", "internetlimit");
    } else if (eventobject.id === "txtMailLimit") {
        onTextChange_ECOMMERCELIMIT(frmEnableInternetTransactionKA.txtMailLimit, frmEnableInternetTransactionKA.sliderMailOrderLimit, "onTextChange", "mailorderlimit");
    }
}
function onClickTbxAmount(eventobject, changedtext) {
    return AS_TextField_d89cd9d4d9804a29b8b9d35051f88f18(eventobject, changedtext);
}

function AS_TextField_d89cd9d4d9804a29b8b9d35051f88f18(eventobject, changedtext) {
    //frmNewTransferKA.txtBox.text=formatAmountwithcomma(frmNewTransferKA.txtBox.text,3);
    gblFeesFrom = "fromAmt";
    if ((frmNewTransferKA.lblFromAccCurr.text !== frmNewTransferKA.lblToAccCurr.text) && !isEmpty(frmNewTransferKA.lblCurrency.text)) {
        getCommisionFee();
    }
    var amount = frmNewTransferKA.txtBox.text;
    if (amount === null || amount === "" || amount == "." || ((Number(amount) < 0.01) && frmNewTransferKA.lblCurrency.text !== "JOD") || ((Number(amount) < 0.001) && frmNewTransferKA.lblCurrency.text === "JOD") || amount === 0.00) {
        frmNewTransferKA.flxInvalidAmountField.setVisibility(true);
        frmNewTransferKA.lblLine3.skin = "sknFlxOrangeLine";
        frmNewTransferKA.lblNext.skin = sknLblNextDisabled;
        frmNewTransferKA.lblNext.setEnabled(false);
        kony.print("Amount is either null or empty");
    } else {
        frmNewTransferKA.flxInvalidAmountField.setVisibility(false);
        frmNewTransferKA.lblLine3.skin = "lblLine0h8b99d8a85f649";
        frmNewTransferKA.lblHiddenAmount.text = frmNewTransferKA.txtBox.text;
        frmNewTransferKA.txtBox.maxTextLength = 20;
        frmNewTransferKA.txtBox.text = formatAmountwithcomma(amount, getDecimalNumForCurr(frmNewTransferKA.lblFromAccCurr.text));
    }
    var Text = frmNewTransferKA.txtDesc.text;
    if (frmNewTransferKA.lblFre.text == "Instant") animateLabel("UP", "lblDescription", Text);
    else {
        animateLabel("UP", "lblNumRecurr", frmNewTransferKA.txtNumRecurrences.text);
    }
}
function AS_Switch_g1e42d0084374b908a3885361ffefb5f(eventobject) {
    if (frmAuthorizationAlternatives.flxQuickBalance.height === "8%") {
        frmAuthorizationAlternatives.flxQuickBalance.height = "40%";
        addDatatoSegQuickBalance(1);
    } else {
        frmAuthorizationAlternatives.flxQuickBalance.height = "8%";
        addDatatoSegQuickBalance(0);
    }
}
//startup.js file
var globalhttpheaders = {};
var appConfig = {
    appId: "BOJMobileBanking",
    appName: "BOJ Mobile",
    appVersion: "1.7.4",
    platformVersion: null,
    serverIp: "10.10.17.222",
    serverPort: "80",
    secureServerPort: "443",
    isDebug: true,
    middlewareContext: "BOJMobileBanking",
    isturlbase: "https://mobileuat.bankofjordan.com/services",
    isMFApp: true,
    appKey: "de868f68e0db476e369ce1d2ec311877",
    appSecret: "3e42682a543882869eaf937bbd29cf84",
    serviceUrl: "https://mobileuat.bankofjordan.com/authService/100000044/appconfig",
    svcDoc: {
        "appId": "fd516bcb-08cb-4f43-a4d3-4a44ed229d61",
        "baseId": "19f965b1-605e-4773-aed9-deeb6ffb80ac",
        "name": "RetailBankingServices",
        "selflink": "https://mobileuat.bankofjordan.com/authService/100000044/appconfig",
        "login": [{
            "type": "oauth2",
            "prov": "LinkedIn",
            "url": "https://mobileuat.bankofjordan.com/authService/100000044",
            "alias": "LinkedIn"
        }, {
            "mandatory_fields": [],
            "type": "basic",
            "prov": "CustomLogin",
            "url": "https://mobileuat.bankofjordan.com/authService/100000044",
            "alias": "CustomLogin"
        }],
        "messagingsvc": {
            "appId": "fd516bcb-08cb-4f43-a4d3-4a44ed229d61",
            "url": "https://mobileuat.bankofjordan.com/kpns/api/v1"
        },
        "integsvc": {
            "BOJCXLead": "https://mobileuat.bankofjordan.com/services/BOJCXLead",
            "BOJprGetFtdIntRate": "https://mobileuat.bankofjordan.com/services/BOJprGetFtdIntRate",
            "BOJIPS": "https://mobileuat.bankofjordan.com/services/BOJIPS",
            "BOJprCreateAddFtd": "https://mobileuat.bankofjordan.com/services/BOJprCreateAddFtd",
            "GetTransactionAndMaster": "https://mobileuat.bankofjordan.com/services/GetTransactionAndMaster",
            "BOJprGetAccTxnMaster": "https://mobileuat.bankofjordan.com/services/BOJprGetAccTxnMaster",
            "BOJprGetTransferLimit": "https://mobileuat.bankofjordan.com/services/BOJprGetTransferLimit",
            "BOJBitlyShortenURL": "https://mobileuat.bankofjordan.com/services/BOJBitlyShortenURL",
            "BOJUpdateFavorBeneficiary": "https://mobileuat.bankofjordan.com/services/BOJUpdateFavorBeneficiary",
            "BOJATMList": "https://mobileuat.bankofjordan.com/services/BOJATMList",
            "BOJBranchList": "https://mobileuat.bankofjordan.com/services/BOJBranchList",
            "BOJSendOTP": "https://mobileuat.bankofjordan.com/services/BOJSendOTP",
            "BOJCnfSiCancellation": "https://mobileuat.bankofjordan.com/services/BOJCnfSiCancellation",
            "BOJGetAccDetailsNode": "https://mobileuat.bankofjordan.com/services/BOJGetAccDetailsNode",
            "BOJGetTransactionDetails": "https://mobileuat.bankofjordan.com/services/BOJGetTransactionDetails",
            "RetailBankingBEServices": "https://mobileuat.bankofjordan.com/services/RetailBankingBEServices",
            "BOJDcDelAccLink": "https://mobileuat.bankofjordan.com/services/BOJDcDelAccLink",
            "BOJprApplyLoanPostpone": "https://mobileuat.bankofjordan.com/services/BOJprApplyLoanPostpone",
            "BOJDcChangeDefaultAcc": "https://mobileuat.bankofjordan.com/services/BOJDcChangeDefaultAcc",
            "BOJDcAddAccLink": "https://mobileuat.bankofjordan.com/services/BOJDcAddAccLink",
            "BOJCardlessAuth": "https://mobileuat.bankofjordan.com/services/BOJCardlessAuth",
            "BOJCancelCardlessRequest": "https://mobileuat.bankofjordan.com/services/BOJCancelCardlessRequest",
            "BOJCardlessTransList": "https://mobileuat.bankofjordan.com/services/BOJCardlessTransList",
            "BOJGetExchRates": "https://mobileuat.bankofjordan.com/services/BOJGetExchRates",
            "BOJGetStatisticsDetails": "https://mobileuat.bankofjordan.com/services/BOJGetStatisticsDetails",
            "BOJCreditCardPayment": "https://mobileuat.bankofjordan.com/services/BOJCreditCardPayment",
            "BOJGetCrCardDetails": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardDetails",
            "BOJGetProfileData": "https://mobileuat.bankofjordan.com/services/BOJGetProfileData",
            "BOJGetCrCardHistTrans": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardHistTrans",
            "BOJPrConfirmJmpPayments": "https://mobileuat.bankofjordan.com/services/BOJPrConfirmJmpPayments",
            "BOJGetCrCardStmt": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardStmt",
            "BOJGetTransferFee": "https://mobileuat.bankofjordan.com/services/BOJGetTransferFee",
            "BOJCancelCreditCard": "https://mobileuat.bankofjordan.com/services/BOJCancelCreditCard",
            "BOJCheckCardPin": "https://mobileuat.bankofjordan.com/services/BOJCheckCardPin",
            "BOJUnClearTxn": "https://mobileuat.bankofjordan.com/services/BOJUnClearTxn",
            "BOJPrGetTdDetails": "https://mobileuat.bankofjordan.com/services/BOJPrGetTdDetails",
            "BOJGetAccountDetails": "https://mobileuat.bankofjordan.com/services/BOJGetAccountDetails",
            "BOJGetEfwDebitAmt": "https://mobileuat.bankofjordan.com/services/BOJGetEfwDebitAmt",
            "BOJGetEfwMappingCodes": "https://mobileuat.bankofjordan.com/services/BOJGetEfwMappingCodes",
            "prAddBeneficiary": "https://mobileuat.bankofjordan.com/services/prAddBeneficiary",
            "BOJGetPpServiceTypes": "https://mobileuat.bankofjordan.com/services/BOJGetPpServiceTypes",
            "prGetBeneficiaryDetails": "https://mobileuat.bankofjordan.com/services/prGetBeneficiaryDetails",
            "BOJGetStandingInstructions": "https://mobileuat.bankofjordan.com/services/BOJGetStandingInstructions",
            "BOJGetBillerList": "https://mobileuat.bankofjordan.com/services/BOJGetBillerList",
            "BOJprGetInRemitDetails": "https://mobileuat.bankofjordan.com/services/BOJprGetInRemitDetails",
            "BOJprGetOutRemitDetails": "https://mobileuat.bankofjordan.com/services/BOJprGetOutRemitDetails",
            "SendPushMessage": "https://mobileuat.bankofjordan.com/services/SendPushMessage",
            "BOJprCcPinReminder": "https://mobileuat.bankofjordan.com/services/BOJprCcPinReminder",
            "BOJPPBillerAccounts": "https://mobileuat.bankofjordan.com/services/BOJPPBillerAccounts",
            "prGetExternalTransferComm": "https://mobileuat.bankofjordan.com/services/prGetExternalTransferComm",
            "BOJApplyDebitCard": "https://mobileuat.bankofjordan.com/services/BOJApplyDebitCard",
            "BojGetPpRegisteredBilling": "https://mobileuat.bankofjordan.com/services/BojGetPpRegisteredBilling",
            "BOJprDcLinkedAccount": "https://mobileuat.bankofjordan.com/services/BOJprDcLinkedAccount",
            "BOJprJmpGetRegisterDetails": "https://mobileuat.bankofjordan.com/services/BOJprJmpGetRegisterDetails",
            "BOJPOSprCcSpendingControl": "https://mobileuat.bankofjordan.com/services/BOJPOSprCcSpendingControl",
            "BOJprDcReqNewPin": "https://mobileuat.bankofjordan.com/services/BOJprDcReqNewPin",
            "BOJprCcSpendingControl": "https://mobileuat.bankofjordan.com/services/BOJprCcSpendingControl",
            "BOJprAccStmtOrder": "https://mobileuat.bankofjordan.com/services/BOJprAccStmtOrder",
            "BOJPushNotification": "https://mobileuat.bankofjordan.com/services/BOJPushNotification",
            "BOJrApplySuppDcNew": "https://mobileuat.bankofjordan.com/services/BOJrApplySuppDcNew",
            "BOJprCcActivation": "https://mobileuat.bankofjordan.com/services/BOJprCcActivation",
            "GoogleAPIs": "https://mobileuat.bankofjordan.com/services/GoogleAPIs",
            "BOJBankList": "https://mobileuat.bankofjordan.com/services/BOJBankList",
            "ManualUserCreation": "https://mobileuat.bankofjordan.com/services/ManualUserCreation",
            "BOJGetWebChrgCardDetails": "https://mobileuat.bankofjordan.com/services/BOJGetWebChrgCardDetails",
            "iWatchBOJPrGetTdDetails": "https://mobileuat.bankofjordan.com/services/iWatchBOJPrGetTdDetails",
            "BOJSendSMSMobile": "https://mobileuat.bankofjordan.com/services/BOJSendSMSMobile",
            "iWatchBOJPrGetLoanAccDetails": "https://mobileuat.bankofjordan.com/services/iWatchBOJPrGetLoanAccDetails",
            "BOJPrfChngSms": "https://mobileuat.bankofjordan.com/services/BOJPrfChngSms",
            "BOJCrdtCrdChngSms": "https://mobileuat.bankofjordan.com/services/BOJCrdtCrdChngSms",
            "BOJCancelDebitCard": "https://mobileuat.bankofjordan.com/services/BOJCancelDebitCard",
            "BOJBeneBranchList": "https://mobileuat.bankofjordan.com/services/BOJBeneBranchList",
            "BOJJmpRegister": "https://mobileuat.bankofjordan.com/services/BOJJmpRegister",
            "BOJRestServices": "https://mobileuat.bankofjordan.com/services/BOJRestServices",
            "BOJApplyWcCardNew": "https://mobileuat.bankofjordan.com/services/BOJApplyWcCardNew",
            "BOJApplySuppCcNew": "https://mobileuat.bankofjordan.com/services/BOJApplySuppCcNew",
            "BOJsendUserName": "https://mobileuat.bankofjordan.com/services/BOJsendUserName",
            "BOJApplyPrimCcReplace": "https://mobileuat.bankofjordan.com/services/BOJApplyPrimCcReplace",
            "BOJDeleteBeneficiary": "https://mobileuat.bankofjordan.com/services/BOJDeleteBeneficiary",
            "BOJDebitCrdTxnList": "https://mobileuat.bankofjordan.com/services/BOJDebitCrdTxnList",
            "BOJJavaServices": "https://mobileuat.bankofjordan.com/services/BOJJavaServices",
            "BOJPrExternalTransfer": "https://mobileuat.bankofjordan.com/services/BOJPrExternalTransfer",
            "BOJPrJmpGetFees": "https://mobileuat.bankofjordan.com/services/BOJPrJmpGetFees",
            "GetAccountListService": "https://mobileuat.bankofjordan.com/services/GetAccountListService",
            "NotificationController": "https://mobileuat.bankofjordan.com/services/NotificationController",
            "BOJChangeSMSNo": "https://mobileuat.bankofjordan.com/services/BOJChangeSMSNo",
            "EFWateercomBillerList": "https://mobileuat.bankofjordan.com/services/EFWateercomBillerList",
            "LoopExample": "https://mobileuat.bankofjordan.com/services/LoopExample",
            "BojUserReg": "https://mobileuat.bankofjordan.com/services/BojUserReg",
            "updateBeneficiary": "https://mobileuat.bankofjordan.com/services/updateBeneficiary",
            "LoopOutRemitDetails": "https://mobileuat.bankofjordan.com/services/LoopOutRemitDetails",
            "CancelCard": "https://mobileuat.bankofjordan.com/services/CancelCard",
            "LoopBulkPaymentPostpaid": "https://mobileuat.bankofjordan.com/services/LoopBulkPaymentPostpaid",
            "BranchATMLocator": "https://mobileuat.bankofjordan.com/services/BranchATMLocator",
            "InformationSupport": "https://mobileuat.bankofjordan.com/services/InformationSupport",
            "ApplyCards": "https://mobileuat.bankofjordan.com/services/ApplyCards",
            "DebitCardList": "https://mobileuat.bankofjordan.com/services/DebitCardList",
            "GetCommission": "https://mobileuat.bankofjordan.com/services/GetCommission",
            "LoopBulkPrepaidPaymentServ": "https://mobileuat.bankofjordan.com/services/LoopBulkPrepaidPaymentServ",
            "OTPController": "https://mobileuat.bankofjordan.com/services/OTPController",
            "LoopBulkPayment": "https://mobileuat.bankofjordan.com/services/LoopBulkPayment",
            "AllCardList": "https://mobileuat.bankofjordan.com/services/AllCardList",
            "ATMPOSLimit": "https://mobileuat.bankofjordan.com/services/ATMPOSLimit",
            "GetNotificationController": "https://mobileuat.bankofjordan.com/services/GetNotificationController",
            "AccountTransactions": "https://mobileuat.bankofjordan.com/services/AccountTransactions",
            "PrePaidValnDebitAmount": "https://mobileuat.bankofjordan.com/services/PrePaidValnDebitAmount",
            "loopCardLinkAccount": "https://mobileuat.bankofjordan.com/services/loopCardLinkAccount",
            "TransferMoneySS": "https://mobileuat.bankofjordan.com/services/TransferMoneySS",
            "ATMLocator": "https://mobileuat.bankofjordan.com/services/ATMLocator",
            "LoopStatistics": "https://mobileuat.bankofjordan.com/services/LoopStatistics",
            "ChangeUnamePwd": "https://mobileuat.bankofjordan.com/services/ChangeUnamePwd",
            "LoopBulkPaymentPrePaid": "https://mobileuat.bankofjordan.com/services/LoopBulkPaymentPrePaid",
            "OTPSMSController": "https://mobileuat.bankofjordan.com/services/OTPSMSController",
            "GetDetails": "https://mobileuat.bankofjordan.com/services/GetDetails",
            "loopCardUnLinkAccount": "https://mobileuat.bankofjordan.com/services/loopCardUnLinkAccount",
            "getTncProfileData": "https://mobileuat.bankofjordan.com/services/getTncProfileData",
            "PushNotification": "https://mobileuat.bankofjordan.com/services/PushNotification",
            "LoopInRemitDetails": "https://mobileuat.bankofjordan.com/services/LoopInRemitDetails",
            "pushNotificationsAuthService": "https://mobileuat.bankofjordan.com/services/pushNotificationsAuthService",
            "BOJBillerAccounts": "https://mobileuat.bankofjordan.com/services/BOJBillerAccounts",
            "BOJGetEfwServtypes": "https://mobileuat.bankofjordan.com/services/BOJGetEfwServtypes",
            "BOJEfwPaymentConfirm": "https://mobileuat.bankofjordan.com/services/BOJEfwPaymentConfirm",
            "BOJGetEfwListBillers": "https://mobileuat.bankofjordan.com/services/BOJGetEfwListBillers",
            "BOJPpPayment": "https://mobileuat.bankofjordan.com/services/BOJPpPayment",
            "BOJGetPpValidation": "https://mobileuat.bankofjordan.com/services/BOJGetPpValidation",
            "BOJprCcUnblockPin": "https://mobileuat.bankofjordan.com/services/BOJprCcUnblockPin",
            "BOJGetRegisteredEstmt": "https://mobileuat.bankofjordan.com/services/BOJGetRegisteredEstmt",
            "BOJEstmtConfirm": "https://mobileuat.bankofjordan.com/services/BOJEstmtConfirm",
            "BOJprChqBookOrder": "https://mobileuat.bankofjordan.com/services/BOJprChqBookOrder",
            "addPrePaidBene": "https://mobileuat.bankofjordan.com/services/addPrePaidBene",
            "BOJApplyPrimaryCcNew": "https://mobileuat.bankofjordan.com/services/BOJApplyPrimaryCcNew",
            "BOJprCcInstantCashback": "https://mobileuat.bankofjordan.com/services/BOJprCcInstantCashback",
            "BOJCnfOpenNewAcc": "https://mobileuat.bankofjordan.com/services/BOJCnfOpenNewAcc",
            "BojprDcActivation": "https://mobileuat.bankofjordan.com/services/BojprDcActivation",
            "BOJGetBillerDetails": "https://mobileuat.bankofjordan.com/services/BOJGetBillerDetails",
            "BOJGetDbCardDetails": "https://mobileuat.bankofjordan.com/services/BOJGetDbCardDetails",
            "BOJPrGetLoanAccDetails": "https://mobileuat.bankofjordan.com/services/BOJPrGetLoanAccDetails",
            "prCcInternetMailorder": "https://mobileuat.bankofjordan.com/services/prCcInternetMailorder",
            "BOJprAccTransfer": "https://mobileuat.bankofjordan.com/services/BOJprAccTransfer",
            "testips": "https://mobileuat.bankofjordan.com/services/testips",
            "BOJprCcEfwPaymentConfirm": "https://mobileuat.bankofjordan.com/services/BOJprCcEfwPaymentConfirm",
            "updateJMPBene": "https://mobileuat.bankofjordan.com/services/updateJMPBene",
            "BOJECMsendimage": "https://mobileuat.bankofjordan.com/services/BOJECMsendimage",
            "BOJprCheckExistIPSCust": "https://mobileuat.bankofjordan.com/services/BOJprCheckExistIPSCust",
            "BOJAliasIPS": "https://mobileuat.bankofjordan.com/services/BOJAliasIPS",
            "BOJEMVParse": "https://mobileuat.bankofjordan.com/services/BOJEMVParse",
            "BOJNationalID": "https://mobileuat.bankofjordan.com/services/BOJNationalID",
            "BOJAutoBillsPay": "https://mobileuat.bankofjordan.com/services/BOJAutoBillsPay",
            "BOJprIPSRegister": "https://mobileuat.bankofjordan.com/services/BOJprIPSRegister",
            "IPSCustomer": "https://mobileuat.bankofjordan.com/services/IPSCustomer",
            "BOJprCcPpPayment": "https://mobileuat.bankofjordan.com/services/BOJprCcPpPayment"
        },
        "reportingsvc": {
            "custom": "https://mobileuat.bankofjordan.com/services/CMS",
            "session": "https://mobileuat.bankofjordan.com/services/IST"
        },
        "Webapp": {
            "url": "https://mobileuat.bankofjordan.com/Share"
        },
        "services_meta": {
            "BOJCXLead": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCXLead",
                "type": "integsvc"
            },
            "BOJprGetFtdIntRate": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprGetFtdIntRate",
                "type": "integsvc"
            },
            "BOJIPS": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJIPS",
                "type": "integsvc"
            },
            "BOJprCreateAddFtd": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCreateAddFtd",
                "type": "integsvc"
            },
            "GetTransactionAndMaster": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GetTransactionAndMaster",
                "type": "integsvc"
            },
            "BOJprGetAccTxnMaster": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprGetAccTxnMaster",
                "type": "integsvc"
            },
            "BOJprGetTransferLimit": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprGetTransferLimit",
                "type": "integsvc"
            },
            "BOJBitlyShortenURL": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJBitlyShortenURL",
                "type": "integsvc"
            },
            "BOJUpdateFavorBeneficiary": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJUpdateFavorBeneficiary",
                "type": "integsvc"
            },
            "BOJATMList": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJATMList",
                "type": "integsvc"
            },
            "BOJBranchList": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJBranchList",
                "type": "integsvc"
            },
            "BOJSendOTP": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJSendOTP",
                "type": "integsvc"
            },
            "BOJCnfSiCancellation": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCnfSiCancellation",
                "type": "integsvc"
            },
            "BOJGetAccDetailsNode": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetAccDetailsNode",
                "type": "integsvc"
            },
            "BOJGetTransactionDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetTransactionDetails",
                "type": "integsvc"
            },
            "RetailBankingBEServices": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/RetailBankingBEServices",
                "type": "integsvc"
            },
            "BOJDcDelAccLink": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJDcDelAccLink",
                "type": "integsvc"
            },
            "BOJprApplyLoanPostpone": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprApplyLoanPostpone",
                "type": "integsvc"
            },
            "BOJDcChangeDefaultAcc": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJDcChangeDefaultAcc",
                "type": "integsvc"
            },
            "BOJDcAddAccLink": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJDcAddAccLink",
                "type": "integsvc"
            },
            "BOJCardlessAuth": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCardlessAuth",
                "type": "integsvc"
            },
            "BOJCancelCardlessRequest": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCancelCardlessRequest",
                "type": "integsvc"
            },
            "BOJCardlessTransList": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCardlessTransList",
                "type": "integsvc"
            },
            "BOJGetExchRates": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetExchRates",
                "type": "integsvc"
            },
            "BOJGetStatisticsDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetStatisticsDetails",
                "type": "integsvc"
            },
            "BOJCreditCardPayment": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCreditCardPayment",
                "type": "integsvc"
            },
            "BOJGetCrCardDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardDetails",
                "type": "integsvc"
            },
            "BOJGetProfileData": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetProfileData",
                "type": "integsvc"
            },
            "BOJGetCrCardHistTrans": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardHistTrans",
                "type": "integsvc"
            },
            "BOJPrConfirmJmpPayments": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrConfirmJmpPayments",
                "type": "integsvc"
            },
            "BOJGetCrCardStmt": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetCrCardStmt",
                "type": "integsvc"
            },
            "BOJGetTransferFee": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetTransferFee",
                "type": "integsvc"
            },
            "BOJCancelCreditCard": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCancelCreditCard",
                "type": "integsvc"
            },
            "BOJCheckCardPin": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCheckCardPin",
                "type": "integsvc"
            },
            "BOJUnClearTxn": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJUnClearTxn",
                "type": "integsvc"
            },
            "BOJPrGetTdDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrGetTdDetails",
                "type": "integsvc"
            },
            "BOJGetAccountDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetAccountDetails",
                "type": "integsvc"
            },
            "BOJGetEfwDebitAmt": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetEfwDebitAmt",
                "type": "integsvc"
            },
            "BOJGetEfwMappingCodes": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetEfwMappingCodes",
                "type": "integsvc"
            },
            "prAddBeneficiary": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/prAddBeneficiary",
                "type": "integsvc"
            },
            "BOJGetPpServiceTypes": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetPpServiceTypes",
                "type": "integsvc"
            },
            "prGetBeneficiaryDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/prGetBeneficiaryDetails",
                "type": "integsvc"
            },
            "BOJGetStandingInstructions": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetStandingInstructions",
                "type": "integsvc"
            },
            "BOJGetBillerList": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetBillerList",
                "type": "integsvc"
            },
            "BOJprGetInRemitDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprGetInRemitDetails",
                "type": "integsvc"
            },
            "BOJprGetOutRemitDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprGetOutRemitDetails",
                "type": "integsvc"
            },
            "SendPushMessage": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/SendPushMessage",
                "type": "integsvc"
            },
            "BOJprCcPinReminder": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcPinReminder",
                "type": "integsvc"
            },
            "BOJPPBillerAccounts": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPPBillerAccounts",
                "type": "integsvc"
            },
            "prGetExternalTransferComm": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/prGetExternalTransferComm",
                "type": "integsvc"
            },
            "BOJApplyDebitCard": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJApplyDebitCard",
                "type": "integsvc"
            },
            "BojGetPpRegisteredBilling": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BojGetPpRegisteredBilling",
                "type": "integsvc"
            },
            "BOJprDcLinkedAccount": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprDcLinkedAccount",
                "type": "integsvc"
            },
            "BOJprJmpGetRegisterDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprJmpGetRegisterDetails",
                "type": "integsvc"
            },
            "BOJPOSprCcSpendingControl": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPOSprCcSpendingControl",
                "type": "integsvc"
            },
            "BOJprDcReqNewPin": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprDcReqNewPin",
                "type": "integsvc"
            },
            "BOJprCcSpendingControl": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcSpendingControl",
                "type": "integsvc"
            },
            "BOJprAccStmtOrder": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprAccStmtOrder",
                "type": "integsvc"
            },
            "BOJPushNotification": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPushNotification",
                "type": "integsvc"
            },
            "BOJrApplySuppDcNew": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJrApplySuppDcNew",
                "type": "integsvc"
            },
            "BOJprCcActivation": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcActivation",
                "type": "integsvc"
            },
            "GoogleAPIs": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GoogleAPIs",
                "type": "integsvc"
            },
            "BOJBankList": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJBankList",
                "type": "integsvc"
            },
            "ManualUserCreation": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/ManualUserCreation",
                "type": "integsvc"
            },
            "BOJGetWebChrgCardDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetWebChrgCardDetails",
                "type": "integsvc"
            },
            "iWatchBOJPrGetTdDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/iWatchBOJPrGetTdDetails",
                "type": "integsvc"
            },
            "BOJSendSMSMobile": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJSendSMSMobile",
                "type": "integsvc"
            },
            "iWatchBOJPrGetLoanAccDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/iWatchBOJPrGetLoanAccDetails",
                "type": "integsvc"
            },
            "BOJPrfChngSms": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrfChngSms",
                "type": "integsvc"
            },
            "BOJCrdtCrdChngSms": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCrdtCrdChngSms",
                "type": "integsvc"
            },
            "BOJCancelDebitCard": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCancelDebitCard",
                "type": "integsvc"
            },
            "BOJBeneBranchList": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJBeneBranchList",
                "type": "integsvc"
            },
            "BOJJmpRegister": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJJmpRegister",
                "type": "integsvc"
            },
            "BOJRestServices": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJRestServices",
                "type": "integsvc"
            },
            "BOJApplyWcCardNew": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJApplyWcCardNew",
                "type": "integsvc"
            },
            "BOJApplySuppCcNew": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJApplySuppCcNew",
                "type": "integsvc"
            },
            "BOJsendUserName": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJsendUserName",
                "type": "integsvc"
            },
            "BOJApplyPrimCcReplace": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJApplyPrimCcReplace",
                "type": "integsvc"
            },
            "BOJDeleteBeneficiary": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJDeleteBeneficiary",
                "type": "integsvc"
            },
            "BOJDebitCrdTxnList": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJDebitCrdTxnList",
                "type": "integsvc"
            },
            "BOJJavaServices": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJJavaServices",
                "type": "integsvc"
            },
            "BOJPrExternalTransfer": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrExternalTransfer",
                "type": "integsvc"
            },
            "BOJPrJmpGetFees": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrJmpGetFees",
                "type": "integsvc"
            },
            "GetAccountListService": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GetAccountListService",
                "type": "integsvc"
            },
            "NotificationController": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/NotificationController",
                "type": "integsvc"
            },
            "BOJChangeSMSNo": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJChangeSMSNo",
                "type": "integsvc"
            },
            "EFWateercomBillerList": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/EFWateercomBillerList",
                "type": "integsvc"
            },
            "LoopExample": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopExample",
                "type": "integsvc"
            },
            "BojUserReg": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BojUserReg",
                "type": "integsvc"
            },
            "updateBeneficiary": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/updateBeneficiary",
                "type": "integsvc"
            },
            "LoopOutRemitDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopOutRemitDetails",
                "type": "integsvc"
            },
            "CancelCard": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/CancelCard",
                "type": "integsvc"
            },
            "LoopBulkPaymentPostpaid": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopBulkPaymentPostpaid",
                "type": "integsvc"
            },
            "BranchATMLocator": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BranchATMLocator",
                "type": "integsvc"
            },
            "InformationSupport": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/InformationSupport",
                "type": "integsvc"
            },
            "ApplyCards": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/ApplyCards",
                "type": "integsvc"
            },
            "DebitCardList": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/DebitCardList",
                "type": "integsvc"
            },
            "GetCommission": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GetCommission",
                "type": "integsvc"
            },
            "LoopBulkPrepaidPaymentServ": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopBulkPrepaidPaymentServ",
                "type": "integsvc"
            },
            "OTPController": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/OTPController",
                "type": "integsvc"
            },
            "LoopBulkPayment": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopBulkPayment",
                "type": "integsvc"
            },
            "AllCardList": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/AllCardList",
                "type": "integsvc"
            },
            "ATMPOSLimit": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/ATMPOSLimit",
                "type": "integsvc"
            },
            "GetNotificationController": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GetNotificationController",
                "type": "integsvc"
            },
            "AccountTransactions": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/AccountTransactions",
                "type": "integsvc"
            },
            "PrePaidValnDebitAmount": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/PrePaidValnDebitAmount",
                "type": "integsvc"
            },
            "loopCardLinkAccount": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/loopCardLinkAccount",
                "type": "integsvc"
            },
            "TransferMoneySS": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/TransferMoneySS",
                "type": "integsvc"
            },
            "ATMLocator": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/ATMLocator",
                "type": "integsvc"
            },
            "LoopStatistics": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopStatistics",
                "type": "integsvc"
            },
            "ChangeUnamePwd": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/ChangeUnamePwd",
                "type": "integsvc"
            },
            "LoopBulkPaymentPrePaid": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopBulkPaymentPrePaid",
                "type": "integsvc"
            },
            "OTPSMSController": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/OTPSMSController",
                "type": "integsvc"
            },
            "GetDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/GetDetails",
                "type": "integsvc"
            },
            "loopCardUnLinkAccount": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/loopCardUnLinkAccount",
                "type": "integsvc"
            },
            "getTncProfileData": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/getTncProfileData",
                "type": "integsvc"
            },
            "PushNotification": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/PushNotification",
                "type": "integsvc"
            },
            "LoopInRemitDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/LoopInRemitDetails",
                "type": "integsvc"
            },
            "pushNotificationsAuthService": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/pushNotificationsAuthService",
                "type": "integsvc"
            },
            "BOJBillerAccounts": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJBillerAccounts",
                "type": "integsvc"
            },
            "BOJGetEfwServtypes": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetEfwServtypes",
                "type": "integsvc"
            },
            "BOJEfwPaymentConfirm": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJEfwPaymentConfirm",
                "type": "integsvc"
            },
            "BOJGetEfwListBillers": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetEfwListBillers",
                "type": "integsvc"
            },
            "BOJPpPayment": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPpPayment",
                "type": "integsvc"
            },
            "BOJGetPpValidation": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetPpValidation",
                "type": "integsvc"
            },
            "BOJprCcUnblockPin": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcUnblockPin",
                "type": "integsvc"
            },
            "BOJGetRegisteredEstmt": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetRegisteredEstmt",
                "type": "integsvc"
            },
            "BOJEstmtConfirm": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJEstmtConfirm",
                "type": "integsvc"
            },
            "BOJprChqBookOrder": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprChqBookOrder",
                "type": "integsvc"
            },
            "addPrePaidBene": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/addPrePaidBene",
                "type": "integsvc"
            },
            "BOJApplyPrimaryCcNew": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJApplyPrimaryCcNew",
                "type": "integsvc"
            },
            "BOJprCcInstantCashback": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcInstantCashback",
                "type": "integsvc"
            },
            "BOJCnfOpenNewAcc": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJCnfOpenNewAcc",
                "type": "integsvc"
            },
            "BojprDcActivation": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BojprDcActivation",
                "type": "integsvc"
            },
            "BOJGetBillerDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetBillerDetails",
                "type": "integsvc"
            },
            "BOJGetDbCardDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJGetDbCardDetails",
                "type": "integsvc"
            },
            "BOJPrGetLoanAccDetails": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJPrGetLoanAccDetails",
                "type": "integsvc"
            },
            "prCcInternetMailorder": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/prCcInternetMailorder",
                "type": "integsvc"
            },
            "BOJprAccTransfer": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprAccTransfer",
                "type": "integsvc"
            },
            "testips": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/testips",
                "type": "integsvc"
            },
            "BOJprCcEfwPaymentConfirm": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcEfwPaymentConfirm",
                "type": "integsvc"
            },
            "updateJMPBene": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/updateJMPBene",
                "type": "integsvc"
            },
            "BOJECMsendimage": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJECMsendimage",
                "type": "integsvc"
            },
            "BOJprCheckExistIPSCust": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCheckExistIPSCust",
                "type": "integsvc"
            },
            "BOJAliasIPS": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJAliasIPS",
                "type": "integsvc"
            },
            "BOJEMVParse": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJEMVParse",
                "type": "integsvc"
            },
            "BOJNationalID": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJNationalID",
                "type": "integsvc"
            },
            "BOJAutoBillsPay": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJAutoBillsPay",
                "type": "integsvc"
            },
            "BOJprIPSRegister": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprIPSRegister",
                "type": "integsvc"
            },
            "IPSCustomer": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/IPSCustomer",
                "type": "integsvc"
            },
            "BOJprCcPpPayment": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/BOJprCcPpPayment",
                "type": "integsvc"
            },
            "RBObjects": {
                "version": "1.0",
                "url": "https://mobileuat.bankofjordan.com/services/data/v1/RBObjects",
                "metadata_url": "https://mobileuat.bankofjordan.com/services/metadata/v1/RBObjects",
                "type": "objectsvc"
            }
        }
    },
    svcDocRefresh: false,
    svcDocRefreshTimeSecs: -1,
    eventTypes: ["FormEntry", "Error", "Crash"],
    url: "https://mobileuat.bankofjordan.com/authService/100000044/appconfig",
    secureurl: "https://mobileuat.bankofjordan.com/authService/100000044/appconfig"
};
sessionID = "";

function appInit(params) {
    skinsInit();
    initializemainSegmentTemplate();
    initializesegacntstatementstmplt();
    initializesegaddnewpaye();
    initializesegBulkPaymentConfirmation();
    initializesegbulkpaymenttmp();
    initializesegCheckReOrderKA();
    initializesegDebitCardTrans();
    initializesegdirectionInfoKA();
    initializesegFAQRow();
    initializesegFAQSection();
    initializesegGetAllIPSAlias();
    initializesegInformation();
    initializesegJMPBene();
    initializesegListViewKA();
    initializesegmanagecardlesstmplt();
    initializesegmanagecardstmplt();
    initializesegmanagepayeetmplt();
    initializesegmentAndroidIcon();
    initializesegmentHeader();
    initializesegmentheadertmplt();
    initializesegmentPayPerson();
    initializesegmentWithChevron();
    initializesegmentWithDoubleLabel();
    initializesegmentWithIcon();
    initializesegmentWithLabel();
    initializesegmentWithoutIcon();
    initializesegmentWithSwitch();
    initializesegMessagesBoldTmplDraftKA();
    initializesegMessagesBoldTmplKA();
    initializesegMessagesTmplKA();
    initializesegphonecnct();
    initializesegSelectProductKA();
    initializesegShowAcc();
    initializesegTmpData();
    initializesegtmpltchecknocolor();
    initializesegtransactionchecktmplt();
    initializesegTransferAddExternal();
    initializetempCurrencySectionHeader();
    initializetempJomopayPopup();
    initializetempNUOClassificationTypesKA();
    initializetempP2PphonePayeeKA();
    initializetempP2PphonePayeeSectionsKA();
    initializetempPayPeopleKA();
    initializetempSectionHeaderP2P();
    initializetmpAccountDetailsScreen();
    initializetmpAccountLandingKA();
    initializetmpAccountSMSDisabled();
    initializetmpAccountSMSEnabled();
    initializetmpAccountsPayBill();
    initializetmpAccountType();
    initializetmpAccountTypes();
    initializetmpAccPrecheckNoTransactions();
    initializetmpAddressSearchResults();
    initializetmpAlert();
    initializetmpAllBeneHeader();
    initializetmpAtmServices();
    initializetmpBudgetListKA();
    initializetmpCalculateFx();
    initializetmpCardNumber();
    initializetmpCardsReorder();
    initializetmpChooseAccountsSettings();
    initializetmpCurrency();
    initializetmpDeviceRegstrationDisabled();
    initializetmpDeviceRegstrationEnabled();
    initializetmpExchangeRates();
    initializetmpFrequrncyData();
    initializetmpJomopaycontacts();
    initializetmpJomopayList();
    initializetmplateCompanyNameKA();
    initializetmpLinkedAccounts();
    initializetmpLocationAtmBranch();
    initializetmplsegLegendKA();
    initializetmpMonths();
    initializetmpNavigationOptKA();
    initializetmpPOT();
    initializetmpProductBundle();
    initializetmpQuickBalanceSeg();
    initializetmpSearchResultsKA();
    initializetmpSegAccountsTran();
    initializetmpSegAccTranNoData();
    initializetmpSegBotUserInput();
    initializetmpSegCardsLanding();
    initializetmpSegLoginMethods();
    initializetmpSegPaymentHistory();
    initializetmpSegRequestStatus();
    initializetmpsegSIList();
    initializetmpServiceTypeDataPrePaid();
    initializetmpTransactionBiller();
    initializetmpTransactionStatus();
    initializetmpTwoLabelsWithHyphen();
    initializetmpVersionKA();
    initializetmSegReleaseNote();
    initializetransactionTemplateColor();
    initializetransactionTemplateNoColor();
    initializetmpAuthHeader();
    initializeiphoneFooter();
    initializetabBarAccounts();
    initializetabBarDeposits();
    initializetabBarMore();
    initializetabBarTransfer();
    initializeTemp0da2fb8b2bfdc47();
    initializemapAtm();
    initializemapATMBranch();
    initializemapATMBranchWithoutLocation();
    frmAccountAlertsKAGlobals();
    frmAccountDetailKAGlobals();
    frmAccountDetailsScreenGlobals();
    frmAccountInfoKAGlobals();
    frmAccountsLandingKAGlobals();
    frmAccountsReorderKAGlobals();
    frmAccountTypeGlobals();
    frmAddExternalAccountHomeGlobals();
    frmAddExternalAccountKAGlobals();
    frmAddNewPayeeKAGlobals();
    frmAddNewPrePayeeKAGlobals();
    frmAlertHistoryGlobals();
    frmAlertsKAGlobals();
    frmApplyNewCardsConfirmKAGlobals();
    frmApplyNewCardsKAGlobals();
    frmATMPOSLimitGlobals();
    frmAuthorizationAlternativesGlobals();
    frmBillerListFormGlobals();
    frmBillsGlobals();
    frmBotSplashGlobals();
    frmBulkPaymentConfirmKAGlobals();
    frmBulkPaymentKAGlobals();
    frmCancelCardKAGlobals();
    frmCardlessSendMessageGlobals();
    frmCardlessTransactionGlobals();
    frmCardLinkedAccountsGlobals();
    frmCardLinkedAccountsConfirmGlobals();
    frmCardOperationsKAGlobals();
    frmCardsAddNicknameGlobals();
    frmCardsLandingKAGlobals();
    frmCardsListPreferencesGlobals();
    frmCardStatementKAGlobals();
    frmChatBotsGlobals();
    frmCheckingJointGlobals();
    frmCheckReOrderListKAGlobals();
    frmCheckReOrderSuccessKAGlobals();
    frmchequeimagesGlobals();
    frmChequeStatusGlobals();
    frmConfirmationCardKAGlobals();
    frmConfirmationPasswordKAGlobals();
    frmConfirmationPersonalDetailsKAGlobals();
    frmConfirmationUserNameKAGlobals();
    frmConfirmCashWithDrawGlobals();
    frmConfirmCashWithDrawQRCodeKAGlobals();
    frmConfirmDepositKAGlobals();
    frmConfirmNPPRegistrationGlobals();
    frmConfirmP2PRegistrationKAGlobals();
    frmConfirmPayBillGlobals();
    frmConfirmPrePayBillGlobals();
    frmConfirmStandingInstructionsGlobals();
    frmConfirmTransferKAGlobals();
    frmCongratulationsGlobals();
    frmContactUsKAGlobals();
    frmCreateCredentialsGlobals();
    frmCreditCardNumberGlobals();
    frmCreditCardPaymentGlobals();
    frmCreditCardsKAGlobals();
    frmCurrencyGlobals();
    frmDefaultLoginMethodKAGlobals();
    frmDepositPayLandingKAGlobals();
    frmDeviceDeRegistrationKAGlobals();
    frmDeviceRegisterationIncorrectPinActicvationKAGlobals();
    frmDeviceregistrarionSuccessKAGlobals();
    frmDeviceRegistrationGlobals();
    frmDeviceRegistrationOptionsKAGlobals();
    frmDirectionsKAGlobals();
    frmEditAccountSettingsGlobals();
    frmEditPayeeKAGlobals();
    frmEditPayeeSuccessBillPayKAGlobals();
    frmEditPayeeSuccessKAGlobals();
    frmEnableInternetTransactionKAGlobals();
    frmEnlargeAdKAGlobals();
    frmEnrolluserLandingKAGlobals();
    frmEnterLocationKAGlobals();
    frmEnterPersonalDetailsKAGlobals();
    frmEPSGlobals();
    frmEstatementConfirmKAGlobals();
    frmEstatementLandingKAGlobals();
    frmFaceIDCaptureGlobals();
    frmFacialAuthEnableGlobals();
    frmFilterSIGlobals();
    frmFilterTransactionGlobals();
    frmFrequencyListGlobals();
    frmFxRateGlobals();
    frmGetIPSAliasGlobals();
    frmInformationDetailsKAGlobals();
    frmInformationKAGlobals();
    frmInstantCashGlobals();
    frmIPSHomeGlobals();
    frmIPSManageBeneGlobals();
    frmIPSRegistrationGlobals();
    frmJoMoPayGlobals();
    frmJomoPayAccountListGlobals();
    frmJOMOPayAddBillerGlobals();
    frmJoMoPayConfirmationGlobals();
    frmJomopayContactsGlobals();
    frmJoMoPayLandingGlobals();
    frmJoMoPayQRConfirmGlobals();
    frmJomopayRegCountryGlobals();
    frmJoMoPayRegistrationGlobals();
    frmLanguageChangeGlobals();
    frmLoanPostponeGlobals();
    frmLocationDetailsKAGlobals();
    frmLocatorATMDetailsKAGlobals();
    frmLocatorBranchDetailsKAGlobals();
    frmLocatorKAGlobals();
    frmLoginAuthSuccessKAGlobals();
    frmLoginKAGlobals();
    frmManageCardLessGlobals();
    frmManageCardsKAGlobals();
    frmManagePayeeKAGlobals();
    frmMoreGlobals();
    frmMoreCheckReorderKAGlobals();
    frmMoreFaqKAGlobals();
    frmMoreForeignExchangeRatesKAGlobals();
    frmMoreInterestRatesKAGlobals();
    frmMoreLandingKAGlobals();
    frmMorePrivacyPolicyKAGlobals();
    frmMoreTermsAndConditionsKAGlobals();
    frmMyAccountSettingsKAGlobals();
    frmMyMoneyListKAGlobals();
    frmNewBillDetailsGlobals();
    frmNewBillKAGlobals();
    frmNewSubAccountConfirmGlobals();
    frmNewSubAccountLandingNewGlobals();
    frmNewTransferKAGlobals();
    frmNewUserOnboardVerificationKAGlobals();
    frmOpenTermDepositGlobals();
    frmOrderCheckBookGlobals();
    frmOrderChequeBookConfirmGlobals();
    frmPayBillHomeGlobals();
    frmPayeeDetailsKAGlobals();
    frmPayeeTransactionsKAGlobals();
    frmPaymentDashboardGlobals();
    frmPendingWithdrawSummaryGlobals();
    frmPickAProductKAGlobals();
    frmPinEntryStep1Globals();
    frmPinEntryStep2Globals();
    frmPinEntrySuccessGlobals();
    frmPostLoginAdvertisementKAGlobals();
    frmPOTGlobals();
    frmPreferredAccountsKAGlobals();
    frmPrePaidPayeeDetailsKAGlobals();
    frmProductBundleGlobals();
    frmReasonsListScreenGlobals();
    frmRecentDepositKAGlobals();
    frmRecentTransactionDetailsKAGlobals();
    frmRegisterUserGlobals();
    frmReleaseNotesKAGlobals();
    frmRequestPinCardGlobals();
    frmRequestStatementAccountsGlobals();
    frmRequestStatementAccountsConfirmGlobals();
    frmrequestStatusGlobals();
    frmScheduledTransactionDetailsKAGlobals();
    frmSearchResultsKAGlobals();
    frmSelectBankNameGlobals();
    frmSelectDetailsBeneGlobals();
    frmSetDefaultPageKAGlobals();
    frmSetLocatorDistanceFilterKAGlobals();
    frmSettingsKAGlobals();
    frmSetupFaceIdSettingsKAGlobals();
    frmSetUpPinSettingsGlobals();
    frmShowAllBeneficiaryGlobals();
    frmSMSNotificationGlobals();
    frmSplashGlobals();
    frmStandingInstructionsGlobals();
    frmStopCardKAGlobals();
    frmSuccessFormKAGlobals();
    frmSuccessFormQRCodeKAGlobals();
    frmTermsAndConditionsKAGlobals();
    frmTermsAndCondtionsGlobals();
    frmtransactionChequeKAGlobals();
    frmTransactionDetailKAGlobals();
    frmTransactionDetailsGlobals();
    frmTransactionDetailsPFMKAGlobals();
    frmTransferStatusGlobals();
    frmUserSettingsMyProfileKAGlobals();
    frmUserSettingsPinLoginKAGlobals();
    frmUserSettingsSiriGlobals();
    frmUserSettingsSIRILIMITGlobals();
    frmUserSettingsTouchIdKAGlobals();
    frmWebChargeGlobals();
    PopupCancelAndReplaceCardGlobals();
    popupCommonAlertGlobals();
    PopupDeviceRegistrationGlobals();
    popupNotificationInfoGlobals();
    popupStopCardGlobals();
    setAppBehaviors();
};

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        APILevel: 6900
    })
};

function themeCallBack() {
    callAppMenu();
    initializeGlobalVariables();
    kony.application.setApplicationInitializationEvents({
        preappinit: AS_AppEvents_b70e24eaba84433d9d1808d9ee81b773,
        init: appInit,
        postappinit: AS_AppEvents_a1f7494243ba459d87b81e142176930a,
        appservice: AS_AppEvents_d7d33e4974bd4754b472728a81aff528,
        showstartupform: function() {
            frmLanguageChange.show();
        }
    });
};

function loadResources() {
    globalhttpheaders = {};
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_LocationSettings"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_toastMsg"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.ND_com_kony"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.ND_com_ip"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_AutoReadSMS"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.NDD_com_kony_keychain"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_BojScan"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.ND_com_siriSetup"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_NSPDFViewer"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_PDFViewer"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_serviceCall"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_QRCodeScanner"
    });
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}

function onSuccess(oldlocalname, newlocalename, info) {
    loadResources();
};

function onFailure(errorcode, errormsg, info) {
    loadResources();
};
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//If default locale is specified. This is set even before any other app life cycle event is called.
kony.i18n.setDefaultLocaleAsync("en", onSuccess, onFailure, null);
// If you wish to debug Application Initialization events, now is the time to
// place breakpoints.
debugger;
function initializetmpAccPrecheckNoTransactions() {
    flxtmpAccPrecheckNoTransactions = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "0dp",
        "id": "flxtmpAccPrecheckNoTransactions",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flexTransparent"
    }, {}, {});
    flxtmpAccPrecheckNoTransactions.setDefaultUnit(kony.flex.DP);
    var lblAccPrecheckNoTrans = new kony.ui.Label({
        "height": "0dp",
        "id": "lblAccPrecheckNoTrans",
        "isVisible": true,
        "left": "100%",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxtmpAccPrecheckNoTransactions.add(lblAccPrecheckNoTrans);
}
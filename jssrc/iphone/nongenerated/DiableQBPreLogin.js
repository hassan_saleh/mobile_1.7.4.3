function DiableQBPLFeature() {
    customAlertPopup(geti18nkey("i18n.cards.Confirmation"), geti18nkey("i18n.common.ConfirmDiableFeature"), callDisableQAccessListPreLogin, popupCommonAlertDimiss, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));
}

function callDisableQAccessListPreLogin() {
    if (kony.sdk.mvvm.isNetworkAvailabile()) {
        ShowLoadingScreen();
        kony.print("Disable the Q feature pre login :::::");
        var AccountList = [];
        var QbalFlag = "N";
        var QTraFlag = "N";
        var AorQFlag = "Q";
        var FlowFlag = "DPL";
        var accObj = {
            AccountNumber: "12345",
            AccName: "12345",
            PreferenceNumber: 1,
            HideShowFlag: "H",
            NickName: ""
        };
        AccountList.push(accObj);
        var langSelected = kony.store.getItem("langPrefObj");
        langSelected = langSelected.toUpperCase();
        kony.print("AccountList::" + JSON.stringify(AccountList));
        var scopeObj = this;
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
        dataObject.addField("custId", custid);
        dataObject.addField("Language", langSelected);
        dataObject.addField("user_id", custid);
        dataObject.addField("FlowFlag", FlowFlag);
        dataObject.addField("QbalFlag", QbalFlag);
        dataObject.addField("QTraFlag", QTraFlag);
        dataObject.addField("AccountList", (JSON.stringify(AccountList)).replace(/"/g, "'"));
        dataObject.addField("AorQFlag", AorQFlag);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("Input params1-->" + JSON.stringify(serviceOptions));
        modelObj.customVerb("SetQuickAccessList", serviceOptions, callDisableQAccessListPreLoginSuccess, callDisableQAccessListPreLoginError);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function callDisableQAccessListPreLoginSuccess(response) {
    kony.print("callDisableQAccessListPreLoginSuccess::: " + JSON.stringify(response));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    if (response.statusCode !== null && response.statusCode !== "" && response.statusCode == "S0024") {
        popupCommonAlertDimiss();
        accountPreviewAnimateOff();
    } else {
        var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
        customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
    }
}

function callDisableQAccessListPreLoginError(err) {
    kony.print("callDisableQAccessListPreLoginError:::" + JSON.stringify(err));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    var errorMessage = geti18nkey("i18n.Generic.InternalServerError");
    customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
}
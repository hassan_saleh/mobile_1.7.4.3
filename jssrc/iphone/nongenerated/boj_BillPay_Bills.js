function getBillDetailsPostpaidBills() {
    if (kony.sdk.isNetworkAvailable()) {
        ShowLoadingScreen();
        var langSelected = kony.store.getItem("langPrefObj");
        var Language = langSelected.toUpperCase();
        var billingNumber = kony.boj.selectedPayee.BillingNumber;
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Payee", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Payee");
        dataObject.addField("custId", custid);
        dataObject.addField("lang", Language);
        dataObject.addField("p_BillerCode", kony.boj.selectedPayee.BillerCode);
        dataObject.addField("p_ServiceType", kony.boj.selectedPayee.ServiceType);
        //alert("Data in svc type "+kony.boj.Biller[kony.boj.selectedBillerType].selectedType.code);
        dataObject.addField("p_BillingNo", billingNumber);
        dataObject.addField("p_BillNo", billingNumber);
        dataObject.addField("p_DateFlag", "N");
        dataObject.addField("p_CustInfoFlag", "Y");
        //     dataObject.addField("p_IdType","NAT");
        //     dataObject.addField("p_ID","9861033442");
        //     dataObject.addField("p_Nation","JO");
        dataObject.addField("p_IncPayments", "N");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("Input params getBillDetailsPostpaidBills-->" + serviceOptions);
        kony.print("Input params getBillDetailsPostpaidBills-->" + JSON.stringify(serviceOptions));
        modelObj.customVerb("GetBillDetails", serviceOptions, getBillDetailsPostpaidBillsSuccess, getBillDetailsPostpaidBillsError);
    } else {
        var Message = getErrorMessage("abcdef");
        customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
    }
}

function getBillDetailsPostpaidBillsSuccess(response) {
    //   alert(JSON.stringify(response));
    //   response =  demoResponsePostpaid;
    //   alert(JSON.stringify(response));
    kony.application.dismissLoadingScreen();
    if (response.ErrorCode === "00000" && response.BillerDetails != undefined) {
        //     alert(JSON.stringify(response));
        kony.print("Success getBillDetailsPostpaid-->" + JSON.stringify(response));
        //     alert("Success!!");
        //     populateDatatopostpaidNewBillDetails(response);
        if (parseFloat(response.BillerDetails[0].dueamount) > 0) {
            populateFrmBills(response);
        } else {
            var Message = "";
            if (response.ErrorCode === "00000" || response.ErrorCode === "0" || response.ErrorCode === 0) {
                Message = geti18Value("i18n.NoDueBilss")
            } else {
                Message = getErrorMessage(response.ErrorCode);
            }
            customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
        }
    } else {
        kony.print("Success getBillDetailsPostpaidBills-->" + JSON.stringify(response));
        var Message = "";
        if (response.ErrorCode === "00000" || response.ErrorCode === "0" || response.ErrorCode === 0) {
            Message = geti18Value("i18n.NoDueBilss")
        } else {
            Message = getErrorMessage(response.ErrorCode);
        }
        customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
        //     kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.transactionFailed"), geti18Value("i18n.Transfer.AnotherTransaction"), "PaymentDashboard",geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard"));
        //     frmCongratulations.show();
    }
}

function getBillDetailsPostpaidBillsError(response) {
    kony.application.dismissLoadingScreen();
    alert(JSON.stringify(response));
    kony.print("Failure getBillDetailsPostpaidBills-->" + JSON.stringify(response));
    alert("Failure!!");
}

function populateFrmBills(res) {
    kony.print("populateFrmBills ::" + JSON.stringify(res));
    //   if(res.feesonbiller !== "false" && res.feesonbiller !== false)
    frmBills.lblFee.text = formatamountwithCurrency(res.BillerDetails[0].feesamt, CURRENCY) + " " + CURRENCY;
    //   else
    //     frmBills.lblFee.text = "0";
    frmBills.lblName.text = kony.boj.selectedPayee.NickName;
    frmBills.lblAccountNumber.text = kony.boj.selectedPayee.BillingNumber;
    frmBills.lblInitial.text = kony.boj.selectedPayee.NickName.substring(0, 2).toUpperCase();
    frmBills.lblHiddenDueAmnt.text = res.BillerDetails[0].dueamount;
    frmBills.dueAmount.text = setDecimal(res.BillerDetails[0].dueamount, 3) + " JOD";
    kony.print("dueAmount ::" + res.BillerDetails[0].dueamount);
    if (res.BillerDetails[0].dueamount !== "" && parseFloat(res.BillerDetails[0].dueamount) !== 0) {
        var amnt = setDecimal(res.BillerDetails[0].dueamount, 3);
        amnt = amnt.replace(/,/g, "");
        frmBills.tbxAmount.text = amnt;
    } else {
        frmBills.tbxAmount.text = "";
    }
    frmBills.minAmount.text = setDecimal(res.BillerDetails[0].lower, 3) + " JOD";
    frmBills.maxAmount.text = setDecimal(res.BillerDetails[0].upper, 3) + " JOD";
    frmBills.issueDate.text = formatDateBill(res.BillerDetails[0].issuedate);
    frmBills.dueDate.text = formatDateBill(res.BillerDetails[0].duedate);
    clearfrmBills();
    gblTModule = "BillPayBillers";
    //   frmBills.show();
    check_numberOfAccounts();
}

function assignDatatoConfirmPostpaidBill(data) {
    var billingNumber = frmBills.lblAccountNumber.text;
    var acc = kony.store.getItem("BillPayfromAcc");
    if (frmBills.btnBillsPayAccounts.text === "t") {
        frmConfirmPayBill.lblAccountNumber.text = acc.accountID;
        frmConfirmPayBill.lblTransferFlag.text = "E";
        frmConfirmPayBill.lblAccBr.text = acc.branchNumber;
        frmConfirmPayBill.lblCurrencyType.text = acc.ccydesc;
        frmConfirmPayBill.lblPaymentMethod.text = "ACTDEB";
    } else {
        frmConfirmPayBill.lblAccountNumber.text = acc.card_num;
        //frmConfirmPayBill.lblCreditCardNumber.text = acc.card_num;
        frmConfirmPayBill.lblTransferFlag.text = "EC";
        frmConfirmPayBill.lblAccBr.text = "";
        frmConfirmPayBill.lblPaymentMethod.text = "CCARD";
    }
    frmConfirmPayBill.lblBillerCode.text = kony.boj.selectedPayee.BillerCode;
    frmConfirmPayBill.lblCustID.text = custid;
    frmConfirmPayBill.lblLang.text = "ara";
    frmConfirmPayBill.lblSvcType.text = kony.boj.selectedPayee.ServiceType;
    frmConfirmPayBill.lblBillingNumber.text = billingNumber;
    frmConfirmPayBill.lblPaidAmnt.text = frmBills.tbxAmount.text;
    frmConfirmPayBill.lblVal.text = frmBills.tbxAmount.text + " " + "JOD"; // hassan 
    frmConfirmPayBill.lblDueAmnt.text = frmBills.lblHiddenDueAmnt.text;
    frmConfirmPayBill.lblAccessChannel.text = "MOBILE";
    frmConfirmPayBill.lblSendSMSflag.text = "N";
    frmConfirmPayBill.lblCustInfoFlag.text = "Y";
    frmConfirmPayBill.lblPaymentStatus.text = "BillNew";
    frmConfirmPayBill.lblFeeAmnt.text = frmBills.lblFee.text.substring(0, frmBills.lblFee.text.length - 4);
    frmConfirmPayBill.lblFeeOnBiller.text = "false";
    frmConfirmPayBill.lblBillNo.text = billingNumber;
    frmConfirmPayBill.lblfcbdrefNo.text = "";
    frmConfirmPayBill.lblPID.text = "";
    frmConfirmPayBill.lblMobileNumber.text = "";
    frmConfirmPayBill.pidType.text = "";
    frmConfirmPayBill.pNation.text = "";
    loadDetailstoConfirmPostpaidBills(data);
}

function loadDetailstoConfirmPostpaidBills(data) {
    kony.print("Data ::" + JSON.stringify(data));
    var acc = kony.store.getItem("BillPayfromAcc");
    var currForm = kony.application.getCurrentForm();
    //var amount = currForm["tbxAmount"].text;
    var amount = "",
        curCode = "JOD"; // hassan 
    if (currForm.btnBillsPayAccounts.text === "t") {
        if (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD") {
            amount = currForm["lblVal"].text;
            curCode = kony.store.getItem("BillPayfromAcc").currencyCode;
        } else amount = currForm["tbxAmount"].text;
    } else amount = currForm["tbxAmount"].text;
    if (frmBills.btnBillsPayAccounts.text === "t") {
        if (acc.AccNickName !== undefined && acc.AccNickName !== "") {
            frmConfirmPayBill.lblSOFAccount.text = acc.AccNickName;
            frmConfirmPayBill.lblSOFAccNum.setVisibility(false);
        } else {
            frmConfirmPayBill.lblSOFAccount.text = acc.accountName;
            frmConfirmPayBill.lblSOFAccNum.setVisibility(true);
        }
        frmConfirmPayBill.lblSOFAccNum.text = acc.hiddenAccountNumber;
    } else {
        frmConfirmPayBill.lblSOFAccount.text = acc.name_on_card;
        frmConfirmPayBill.lblSOFAccNum.text = acc.hiddenAccountNumber;
    }
    // hassan
    //   if (kony.store.getItem("BillPayfromAcc").currencyCode !== "JOD")
    // // 	 frmConfirmPayBill.flxLocalAmount.isVisible=true;
    //   else
    frmConfirmPayBill.flxLocalAmount.isVisible = false;
    var nicknm = kony.boj.selectedPayee.NickName;
    kony.print("nicknm in postpaid is " + nicknm)
    if (nicknm !== undefined && nicknm !== "" && nicknm !== null && nicknm != "0") {
        frmConfirmPayBill.lblBillerCategory.text = nicknm;
        frmConfirmPayBill.initialsCategory.text = nicknm.substring(0, 2).toUpperCase();
    } else {
        frmConfirmPayBill.lblBillerCategory.text = kony.boj.selectedPayee.billerName;
        frmConfirmPayBill.initialsCategory.text = kony.boj.selectedPayee.billerName.substring(0, 2).toUpperCase();
    }
    frmConfirmPayBill.flxBillN.isVisible = false;
    frmConfirmPayBill.lblBillerName.text = "";
    frmConfirmPayBill.lblServiceType.text = kony.boj.selectedPayee.ServiceType;
    frmConfirmPayBill.lblBillerNumber.text = frmBills.lblAccountNumber.text;
    frmConfirmPayBill.lblConfirmDueAmount.text = formatamountwithCurrency(frmBills.lblHiddenDueAmnt.text.replace(/,/g, ""), "JOD") + " JOD";
    frmConfirmPayBill.lblFeeAmount.text = formatamountwithCurrency(frmBills.lblFee.text, "JOD") + " JOD";
    //frmConfirmPayBill.lblPaidAmount.text = formatamountwithCurrency(amount,"JOD")+" JOD";
    frmConfirmPayBill.lblPaidAmount.text = formatamountwithCurrency(amount, curCode) + "  " + curCode; // hassant
    frmConfirmPayBill.lblTotalAmount.text = formatamountwithCurrency(parseFloat(data.p_debit_amt), "JOD") + " JOD";
    frmConfirmPayBill.show();
}

function validate_MINMAXAMOUNTBILLPAYMENT() {
    try {
        var split = null;
        var currForm = kony.application.getCurrentForm();
        var min_amt = currForm.minAmount.text;
        min_amt = min_amt.substring(0, (min_amt.length - 4)) + "";
        split = min_amt.split(".");
        split[0] = split[0].replace(/\,/g, '');
        min_amt = split[0] + "." + split[1];
        split = null;
        min_amt = parseFloat(min_amt);
        var max_amt = currForm.maxAmount.text;
        max_amt = max_amt.substring(0, (max_amt.length - 4)) + "";
        split = max_amt.split(".");
        split[0] = split[0].replace(/\,/g, "");
        max_amt = split[0] + "." + split[1];
        split = null;
        max_amt = parseFloat(max_amt);
        var amount = currForm.tbxAmount.text + "";
        split = amount.split(".");
        split[0] = split[0].replace(/\,/g, '');
        amount = split[0] + "." + split[1];
        amount = parseFloat(amount);
        if ((min_amt > amount)) return "min";
        else if ((amount > max_amt)) return "max";
    } catch (e) {
        kony.print("Exception_validate_MINMAXAMOUNTBILLPAYMENT ::" + e);
    }
}

function restrictoThreeDecimals(value) {
    if (!isEmpty(value)) {
        value = value + "";
        if (value.indexOf(".") !== -1) {
            var splitValue = value.split('.');
            if (!isEmpty(splitValue[1])) {
                if (splitValue[1].length > 3) {
                    value = value.substring(0, value.length - 1);
                }
            }
        }
    }
    return value;
}